from users.serializers import UserSerializer
from rest_framework_jwt.utils import jwt_payload_handler


def jwt_custom_payload_handler(user):
    payload = jwt_payload_handler(user)
    payload = {
        'https://hasura.io/jwt/claims': {
            'x-hasura-allowed-roles': ['editor', 'user'],
            'x-hasura-default-role': 'user',
            'x-hasura-user-id': str(user.pk),
        }
    }

    return payload

def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token
    }
